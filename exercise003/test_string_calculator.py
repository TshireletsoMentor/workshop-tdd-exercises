import unittest


class MyTestCase(unittest.TestCase):
    def test_empty(self):
        """
        Tests that it returns 0 if empty string is provided
        """
        self.fail('Test not implemented yet')

    def test_single_digit(self):
        """
        Tests that it returns the digit provided as sum, if only one digit in string
        """
        self.fail('Test not implemented yet')

    def test_invalid_digit(self):
        """
        Tests that it raises appropriate error if non-digit characters are provided
        """
        self.fail('Test not implemented yet')

    #TODO: add more tests for other scenarios


if __name__ == '__main__':
    unittest.main()
